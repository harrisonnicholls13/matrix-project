#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <limits.h>

// Prefixes to print messages (with colors!)
#define PRT_SPACE		    "---------  "
#define PRT_MSG			    "\033[0;32m[Message]  \033[0m"  // General message
#define PRT_PRMPT		    "\033[0;36m[Command]  \033[0m"  // Prompt
#define PRT_QUERY		    "\033[0;34m[ Query ]  \033[0m"  // Query for input
#define PRT_WARN		    "\033[0;33m[Warning]  \033[0m"  // Warning 
#define PRT_ERROR		    "\033[0;31m[ Error ]  \033[0m"  // Error
#define STRLEN              128

// Macros for neater code
#define STREQ(s,x)          ( strcmp(s,x) == 0 ? 1 : 0)         // Checks if strings are equal
#define MADR(m,d,i,j)       ( m[(i)*(d)+(j)] )                  // Addresses element of 2d array when stored in 1d
#define EVEN(x)             ( ((x) % 2) == 0 ? 1 : 0)           // Check if x is even

// LU definitions
#define SUCC        EXIT_SUCCESS
#define FAIL        EXIT_FAILURE
#define LU_TOL      1e-32


// Define guard
#ifndef GUARD
#define GUARD

// Prototype functions....
double       frand(double min, double max);
double      getTime(void);
void        printm(double* m, int d);
void        divm(double* m, int d, double s);
void        swapRows(double* m, int d, double* mswapped, int r1, int r2);
void        fSub(double* l, int d, double* x, double* b);
void        bSub(double* u, int d, double* x, double* b);
void        saveFile(char* filename, double* m, int d);
double*      loadFile(char* filename, int* d);
double       bf_det(double* m, int d);
double*      bf_inv(double* m, int d);
double       lu_det(double* m, int d, bool pr);
double*      lu_inv(double* m, int d);


#endif