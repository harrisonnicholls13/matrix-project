#include "mp.h"

/**
 * Get random double in range min to max
 * @param min       Minimum of bound of range
 * @param max       Maximum of bound of range
 */
double frand( double min, double max ) {
    double scale = rand() / (double) RAND_MAX; 
    return min + scale * ( max - min );
}

/**
 * Get current system time (in nanoseconds)
 * @return      Time (double precision)
 */
double getTime(void) {
    struct timespec ts;
    timespec_get(&ts, TIME_UTC);
    long t = (long)ts.tv_sec * 1000000000L + ts.tv_nsec;
    return ((double) t / (double) 1000000.0);
}

/**
 * Divide array 'm' of dimension 'd' by scalar 's'
 * @param m             Matrix to calculate the inverse of
 * @param d             Dimensionality of matrix
 * @param s             Divisor
 */
void divm(double* m, int d, double s){
    if (m == NULL) {
        printf(PRT_ERROR"Cannot divide NULL array \n");
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < d*d; i++) {
        m[i] = (double) m[i] / (double) s;
    }
}

/**
 * Print matrix to stdout
 * @param m             Matrix to calculate the inverse of
 * @param d             Dimensionality of matrix
 */
void printm(double* m, int d) {
    for (int i = 0; i < d; i++) {
        printf(PRT_MSG);
        for (int j = 0; j < d; j++) {
            printf("%+-7.03g  ",MADR(m,d,i,j));
        }
        printf("\n");
    }
}

/**
 * Swap rows of a matrix
 * @param m             Original matrix
 * @param d             Dimensionality of matrix
 * @param mswapped      Matrix with swapped rows
 * @param r1            Row 1 (index)
 * @param r2            Row 2 (index)
 */
void swapRows(double* m, int d, double* mswapped, int r1, int r2) {
    if (m == NULL || mswapped == NULL) {
        printf(PRT_ERROR"Error in swapRows: matrix has a null pointer\n");
        exit(EXIT_FAILURE);
    }

    if ( (r1>=d) || (r2>=d) ) {
        printf(PRT_ERROR"Error in swapRows: row out of range \n");
        exit(EXIT_FAILURE);
    }

    // Copy matrix
    memcpy(mswapped,m,d*d*sizeof(double));

    // For each column in the rows, swap their values
    for (int i = 0; i < d; i++) {
        MADR(mswapped,d,r2,i) = MADR(m,d,r1,i);
        MADR(mswapped,d,r1,i) = MADR(m,d,r2,i);
    }

}

/**
 * Perform forward substitution
 * @param l             Matrix (lower triangular)
 * @param d             Dimensionality of matrix
 * @param y             Variable vector (to solve for)
 * @param b             Constants vector 
 */
void fSub(double* l, int d, double* y, double* b) {
    if (l == NULL || y == NULL || b == NULL) {
        printf(PRT_ERROR"Error in fSub: argument has null pointer \n");
        exit(EXIT_FAILURE);
    }

    for (int i = 1; i <= d; i++) {
        double s = 0.0F;
        for (int j = 1; j <= (i-1); j++) {
            s += (MADR(l,d,i-1,j-1) * y[j-1]);
        }
        y[i-1] = (b[i-1] - s) / MADR(l,d,i-1,i-1);
    }

}

/**
 * Perform backward substitution
 * @param u             Matrix (upper triangular)
 * @param d             Dimensionality of matrix
 * @param x             Variable vector (to solve for)
 * @param y             Constants vector 
 */
void bSub(double* u, int d, double* x, double* y) {
    if (u == NULL || x == NULL || y == NULL) {
        printf(PRT_ERROR"Error in bSub: argument has null pointer \n");
        exit(EXIT_FAILURE);
    }

    for (int i = d-1; i > -1; i--){
        double s = 0.0F;
        for (int j = i+1; j < d; j++) {
            s += MADR(u,d,i,j) * x[j];
        }
        x[i] = (y[i] - s) / MADR(u,d,i,i);
    }

}

