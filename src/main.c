#include "mp.h"

int main() {

    // User input variables
    int         do_exit     = 0;        // Should exit at end of loop
    char        input[128]  = "";
    const char  cmd_dlm[2]  = " ";      // Delimeter
    char**      cmd_arr     = NULL;     // Stores user commands
    char*       cmd_p       = NULL;     
    int         cmd_n       = 0;        // Length of user command

    // Current matrix
    double*      mat         = NULL;     // Main matrix - stored as 1d for clarity
    double*      inv         = NULL;     // Inverse of main matrix
    double       det         = 0.0;      // Determinant of current matrix
    int         mat_dim     = 0;        // Dimensionality of current matrix
    double       mat_sv      = 0.0;      // Current set value
    double       rand_min    = 0.0;      // Random number interval minimum
    double       rand_max    = 1.0;      // Random number interval maximum
    double       rand_a      = 0.0;      // Random number interval input A
    double       rand_b      = 1.0;      // Random number interval input B

    // Run status
    bool         timer       = false;    // Timer enabled?
    double       t0          = 0.0F;     // Calculation start time
    double       t1          = 0.0F;     // Calculation end time


    // Welcome message
    printf(PRT_MSG"\n");
    printf(PRT_MSG"          Matrix Manipulation Program by Harrison Nicholls \n");
    printf(PRT_MSG"┌─────────────────────────────────────────────────────────────────┐ \n");
    printf(PRT_MSG"│ This program calculates the inverse and determinant of square   │ \n");
    printf(PRT_MSG"│ NxN matrices using a choice of methods. It uses a prompt-like   │ \n");
    printf(PRT_MSG"│ interface, where the user instructs the program using commands  │ \n");
    printf(PRT_MSG"│ and arguments. Type \'help\' into the prompt for more information.│ \n");
    printf(PRT_MSG"└─────────────────────────────────────────────────────────────────┘ \n");
    printf(PRT_MSG"\n");

    // Seed random number generator
    srand(time(NULL));

    // Main prompt loop - take commands and parse them
    while(!do_exit) {
        // Read prompt
        printf(PRT_PRMPT);
        scanf(" %127[^\n]",input);
        fflush(stdin);

        // Reset variables from last input
        free(cmd_arr);
        cmd_arr = NULL;
        cmd_p = NULL;
        cmd_n = 0;
        // Loop through input
        cmd_p = strtok(input,cmd_dlm);
        while(cmd_p) {
            cmd_arr = realloc(cmd_arr, sizeof(char*) * ++cmd_n);
            if (cmd_arr == NULL) {
                printf(PRT_ERROR"Failed to allocate memory for command \n");
                exit(EXIT_FAILURE);
            }
            cmd_arr[cmd_n - 1] = cmd_p;
            cmd_p = strtok(NULL,cmd_dlm);
        }
        cmd_arr = realloc(cmd_arr, sizeof(char*) * (cmd_n + 1));
        cmd_arr[cmd_n] = 0;

        // Handle command supplied by user
        if ( STREQ(cmd_arr[0],"new") && (cmd_n == 2) ){
            // Initialise new matrix in memory of given dimensionality

            mat_dim = (int) strtol(cmd_arr[1],NULL,10);
            if (mat_dim < 1) {
                printf(PRT_WARN"Matricies must have a dimensionality > 0 \n");
                continue;
            }
            printf(PRT_MSG"New matrix of dimension %d \n",mat_dim);
            if (mat != NULL) {
                free(mat); mat = NULL;
            }
            if (inv != NULL) {
                free(inv); inv = NULL;
            }
            mat = (double*) calloc(mat_dim*mat_dim, sizeof(double));
            if (mat == NULL) {
                printf(PRT_ERROR"Failed to allocate memory for matrix \n");
                exit(EXIT_FAILURE);
            }

        } else if ( STREQ(cmd_arr[0],"fill") && (cmd_n == 2)) {
            if(mat == NULL) { printf(PRT_WARN"No matrix initialised; type 'help' for help \n"); continue; }
            // Fill current matrix with given value

            mat_sv = (double) strtof(cmd_arr[1],NULL);
            printf(PRT_MSG"Filling matrix with value %g \n",mat_sv);
            for (int i = 0; i < mat_dim*mat_dim; i++) {
                mat[i] = mat_sv;
            }
            
        } else if ( STREQ(cmd_arr[0],"setup") ) {
            if(mat == NULL) { printf(PRT_WARN"No matrix initialised; type 'help' for help \n"); continue; }
            // Manually edit matrix by querying user for input
            
            printf(PRT_MSG"Procedurally enter matrix elements... \n");
            for (int i = 0; i < mat_dim; i++) {
                for (int j = 0; j < mat_dim; j++) {
                    printf(PRT_QUERY"(%d,%d) = ",i+1,j+1);
                    if (scanf("%lg",&mat_sv) != 1 ) {
                        // exit if invalid
                        printf(PRT_ERROR"Invalid input \n");
                        exit(EXIT_FAILURE);
                    }
                    MADR(mat,mat_dim,i,j) = mat_sv;
                }
            }
            printf(PRT_MSG"Matrix setup done \n");
        
        } else if ( STREQ(cmd_arr[0],"rand") && (cmd_n == 3) ) {
            if(mat == NULL) { printf(PRT_WARN"No matrix initialised; type 'help' for help \n"); continue; }
            // Fill matrix with random numbers in given interval
            rand_a = (double) strtof(cmd_arr[1],NULL);
            rand_b = (double) strtof(cmd_arr[2],NULL);
            rand_min = fmin(rand_a,rand_b);
            rand_max = fmax(rand_a,rand_b);

            printf(PRT_MSG"Filling matrix with random numbers in range %g -> %g \n",rand_min, rand_max);

            for (int i = 0; i < mat_dim*mat_dim; i++) {
                mat[i] = frand(rand_min, rand_max);
            }

        } else if ( STREQ(cmd_arr[0],"diag") && (cmd_n == 2)) {
            if(mat == NULL) { printf(PRT_WARN"No matrix initialised; type 'help' for help \n"); continue; }
            // Set diagonal elements to given value

            mat_sv = (double) strtof(cmd_arr[1],NULL);
            printf(PRT_MSG"Setting diagonal elements to value %g \n",mat_sv);
            for (int i = 0; i < mat_dim; i++) {
                MADR(mat,mat_dim,i,i) = mat_sv;
            }
            
        } else if ( STREQ(cmd_arr[0],"inv") && (cmd_n == 2) ) {
            if(mat == NULL) { printf(PRT_WARN"No matrix initialised; type 'help' for help \n"); continue; }
            // Calculate inverse of current matrix

            if (inv != NULL) {
                free(inv); inv = NULL;
            }

            if ( STREQ(cmd_arr[1],"bf") ) {
                t0 = getTime();
                inv = bf_inv(mat,mat_dim);
                t1= getTime();
            } else if ( STREQ(cmd_arr[1],"lu") ) {
                t0 = getTime();
                inv = lu_inv(mat,mat_dim);
                t1 = getTime();
            } else {
                printf(PRT_WARN"Invalid method; choose either 'bf' for brute-force, or 'lu' for LU decomposition. PLU not supported. \n");
                continue;
            }

            printm(inv,mat_dim);

            if (timer)
                printf(PRT_MSG"Calculation took %g ms\n",t1-t0);

        } else if ( STREQ(cmd_arr[0],"det") && (cmd_n == 2)) {
            if(mat == NULL) { printf(PRT_WARN"No matrix initialised; type 'help' for help \n"); continue; }
            // Calculate determinant of current matrix

            if ( STREQ(cmd_arr[1],"bf") ) {
                t0 = getTime();
                det = bf_det(mat,mat_dim);
                t1 = getTime();
            } else if ( STREQ(cmd_arr[1],"lu") ) {
                t0 = getTime();
                det = lu_det(mat,mat_dim, false);
                t1 = getTime();
            } else if ( STREQ(cmd_arr[1],"plu") ) {
                t0 = getTime();
                det = lu_det(mat,mat_dim, true);
                t1 = getTime();
            } else {
                printf(PRT_WARN"Invalid method; choose either 'bf' for brute-force, or 'lu' for LU decomposition, or 'plu' for PLU decomposition \n");
                continue;
            }

            if (det != NAN) {
                printf(PRT_MSG"Found det = %.4g \n",det);
                if (timer)
                    printf(PRT_MSG"Calculation took %g ms\n",t1-t0);
            }
            
        } else if ( STREQ(cmd_arr[0],"print") ) {
            if(mat == NULL) { printf(PRT_WARN"No matrix initialised; type 'help' for help \n"); continue; }
            // Print currently stored matrix to stdout

            printm(mat,mat_dim);
            
        } else if ( STREQ(cmd_arr[0],"timer") ) {
            // Toggle timer on and off

            timer = !timer;
            if (timer) {
                printf(PRT_MSG"Timer functionality enabled \n");
            } else {
                printf(PRT_MSG"Timer functionality disabled \n");
            }

        } else if ( STREQ(cmd_arr[0],"load") && (cmd_n == 2) ) {
            // Subprogram 'load'
            if (mat != NULL) {
                free(mat); mat = NULL;
            }
            mat = loadFile(cmd_arr[1],&mat_dim);

        } else if ( STREQ(cmd_arr[0],"save") ) {
            // Subprogram 'save'
            if (mat == NULL) {
                printf(PRT_WARN"Cannot save matrix; no matrix stored \n");
                continue;
            }
            saveFile(cmd_arr[1],mat,mat_dim);

        } else if ( STREQ(cmd_arr[0],"exit") || STREQ(input,"exit") ) {
            // Exit program
            do_exit = 1;

        } else if ( strcmp(cmd_arr[0],"help") == 0 ) {
            //print help menu
            printf(PRT_MSG"Available commands are as follows...\n");
            printf(PRT_MSG"new [N] \n");
            printf(PRT_MSG"\t Creates a new NxN matrix initialised with zeroes \n");
            printf(PRT_MSG"fill [X] \n");
            printf(PRT_MSG"\t Fills current matrix with value X \n");
            printf(PRT_MSG"setup \n");
            printf(PRT_MSG"\t Begins dialogue for setting up matrix manually \n");
            printf(PRT_MSG"rand [X] [Y] \n");
            printf(PRT_MSG"\t Fills current matrix with random real numbers in range X to Y \n");
            printf(PRT_MSG"diag [X] \n");
            printf(PRT_MSG"\t Sets diagonal elements of current matrix to value X \n");
            printf(PRT_MSG"invert [M] \n");
            printf(PRT_MSG"\t Calculates inverse of current matrix using method M (can be either: bf, lu) \n");
            printf(PRT_MSG"det [M] \n");
            printf(PRT_MSG"\t Calculates determinant of current matrix using method M (can be either: bf, lu) \n");
            printf(PRT_MSG"print \n");
            printf(PRT_MSG"\t Prints the current matrix to stdout \n");
            printf(PRT_MSG"timer \n");
            printf(PRT_MSG"\t Toggle measurement of calculation time \n");
            printf(PRT_MSG"load [file] \n");
            printf(PRT_MSG"\t Loads matrix in [file] into memory \n");
            printf(PRT_MSG"save [file] \n");
            printf(PRT_MSG"\t Saves current matrix into [file] \n");
            printf(PRT_MSG"help \n");
            printf(PRT_MSG"\t Prints this help menu \n");
            printf(PRT_MSG"exit \n");
            printf(PRT_MSG"\t Exits the program\n");
            printf(PRT_MSG"\n");

        } else {
            // catch
            printf(PRT_WARN"Invalid or improperly formatted command. Type 'help' for help \n");
        }

    }

    // Tidy up and exit
    if (mat != NULL) {
        free(mat); mat = NULL;
    }
    if (inv != NULL) {
        free(inv); inv = NULL;
    }
    printf(PRT_MSG"Goodbye \n");
    return EXIT_SUCCESS;
}

