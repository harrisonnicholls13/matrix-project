#include "mp.h"

/**
 * Calculate the determinant of a matrix using a brute-force method (recursive Laplace Expansion)
 * @param m             Matrix to calculate the determinant of
 * @param d             Dimensionality of matrix
 * @return              Determinant of matrix
 */
double bf_det(double* m, int d) {

    // Base cases
    if (d == 1) {
        return m[0];
    } else if (d == 2) {
        return (m[0] * m[3]) - (m[1] * m[2]);
    }

    // Variables required for code below
    double det   = 0.0f;      // Total det
    int k = 0;               // Minor counter 
    int r, c    = 0;         // Row, col

    // Calculate determinant for higher cases (recursively)
    for (int i = 0; i < d; i++) { // Loop over top row
        if (MADR(m,d,0,i) == 0 ) continue; // check if element is zero, for speed
        double u[(d-1)*(d-1)]; // Declare minor matrix
        k = 0;
        for (r = 1; r < d; r++) {  // Ignore first row
            for (c = 0; c < d; c++) {
                if (c == i) continue; // Ignore this column
                // Copy to minor matrix
                u[k] = MADR(m,d,r,c);
                k++;
            }
        }

        // Add new factor to determinant and call this function
        // again for a new-submatrix (minor). Account for +/-
        // pattern using EVEN() macro
        det += (-1 + 2*EVEN(i)) * MADR(m,d,0,i) * bf_det(u,(d-1));
    }

    return det;
}

/**
 * Calculate the inverse of a matrix using a brute-force method (based on a Laplace Expansion)
 * @param m             Matrix to calculate the inverse of
 * @param d             Dimensionality of matrix
 * @return              Inverse matrix
 */
double* bf_inv(double* m, int d) {

    double* inv = calloc(d*d, sizeof(double));
    if (inv == NULL) {
        printf(PRT_ERROR"Failed to allocate memory for inverse matrix \n");
        exit(EXIT_FAILURE);
    }

    if (d == 1) {
        inv[0] = 1.0f / m[0];
        return inv;
    }
    
    // Initialise variable 'mco' for storing matrix of cofactors and 'sub' for submatrix
    double* mco = calloc(d*d, sizeof(double));
    double* sub = calloc((d-1)*(d-1),sizeof(double));
    if (mco == NULL || sub == NULL) {
        printf(PRT_ERROR"Failed to allocate memory for matrix of cofactors \n");
        exit(EXIT_FAILURE);
    }
    // Initialise counter variables
    int r, c = 0;
    int ui, uj = 0;
    for (int i = 0; i < d; i++) { // Loop over rows in 'm'
        for (int j = 0; j < d; j++) { // Loop over values in each row of 'm'
            ui = 0; uj = 0;
            for (r = 0; r < d; r++) {
                if (r == i) continue; // Skip row
                for (c = 0; c < d; c++) {
                    if (c == j) continue; // Skip column
                    MADR(sub,(d-1),ui,uj) = MADR(m,d,r,c); // Copy data to 'u'
                    uj++;
                }
                ui++;
                uj = 0;
            }
            MADR(mco,d,i,j) = (-1 + 2*EVEN(i+j)) * bf_det(sub,d-1); // Calculate cofactor
        }
    }
    free(sub); sub = NULL;

    // Transpose matrix of cofactors -> adjoint
    // store result in 'inv' and free 'mco'
    for (int i = 0; i < d; i++) {
        for (int j = 0; j < d; j++) {
            MADR(inv, d, i, j) = MADR(mco, d, j, i);
        }
    }
    free(mco); mco = NULL;

    // Divide adj (stored in 'inv') by determinant to get inverse 
    divm(inv, d, bf_det(m, d));

    return inv;
}

/**
 * (Private) Perform LU decomposition via Crout's method
 * @param m             Matrix to calculate the inverse of
 * @param d             Dimensionality of matrix
 * @param l             L matrix (pointer)
 * @param u             U matrix (pointer)
 */
bool _lu_decomp(double* m, int d, double* l, double* u, bool pr, int* swaps) {

    double   s       = 0.0F; // Sum
    double   denom   = 0.0F; // Denominator
    bool    status  = FAIL; // Status of decomposition

    // Declare temp matrix to stored permuted versions of 'm'
    // This is to avoid overwriting the original matrix 'm'
    double   mp[d*d];
    memcpy(mp, m, d*d*sizeof(double));

    *swaps = 0;
    int swap1 = 0;
    int swap2 = 1;

    // Can't swap rows of matrix with dimensionality of unity
    if (d == 1) {
        pr = false;
    }

    do {
        for (int i = 0; i < d; i++) {
            // Upper matrix
            for (int k = 0; k < d; k++) {
                // Find u_ik
                s = 0.0F;
                for (int j = 0; j < i; j++) {
                    s += MADR(l,d,i,j) * MADR(u,d,j,k);
                }
                MADR(u,d,i,k) = MADR(mp,d,i,k) - s;
            
            }

            denom = MADR(u,d,i,i);
            if (fabs(denom) <= LU_TOL) {
                status = FAIL;
                break;
            } else {
                status = SUCC;
            }

            // Lower matrix
            for (int k = i; k < d; k++) {
                if (i == k) {
                    MADR(l,d,i,i) = 1.0F;
                } else {
                    // Find l_ki
                    s = 0.0F;
                    for (int j = 0; j < i; j++) {
                        s += MADR(l,d,k,j) * MADR(u,d,j,i);
                    }
                    MADR(l,d,k,i) = ( MADR(mp,d,k,i) - s) / denom;
                }
            }
        }

        if (pr && (status == FAIL)) {
            *swaps += 1;
            swapRows(m,d,mp,swap1,swap2);
            swap2++;
            if (swap2 == d) {
                swap2 = 0;
                swap1++;
            }
            if (swap1 == d) {
                break;
            }
        }

    } while ( (status == FAIL) && pr );

    return status;

}

/**
 * Calculate the determinant of a matrix using the LU method
 * @param m             Matrix to calculate the inverse of
 * @param d             Dimensionality of matrix
 * @param pr            Permute matrix rows on failure (true/false)
 * @return              Determinant of matrix
 */
double lu_det(double* m, int d, bool pr) {

    double   det     = 0.0F;     // Determinant
    int     swaps   = 0;        // Number of row swaps
    double   dp_l    = 1.0F;     // Product of diagonal of L
    double   dp_u    = 1.0F;     // Product of diagonal of U
    bool    status  = FAIL;     // Exit code for decomposition

    // Allocate memory
    double* l = (double*) calloc(d*d,sizeof(double));
    double* u = (double*) calloc(d*d,sizeof(double));

    if (l == NULL || u == NULL) {
        printf(PRT_ERROR"Failed to allocate memory for LU matricies \n");
        exit(EXIT_FAILURE);
    }

    // Perform LU decomposition(s)
    status = _lu_decomp(m,d,l,u,pr,&swaps);

    if (status == FAIL) {
        printf(PRT_WARN"LU decomposition failed after %d row permutations. Consider using method=PLU. \n", swaps);
        return NAN;
    } 
    
    // Find diagonal products
    for (int i = 0; i < d; i++) {
        dp_l *= MADR(l,d,i,i);
        dp_u *= MADR(u,d,i,i);
    }

    // Find determinant
    det = (-1 + 2*(swaps==0)) * dp_l * dp_u;

    // Tidy up
    free(l); l = NULL;
    free(u); u = NULL;

    return det;

}

/**
 * Calculate the inverse of a matrix using LU decomposition (and back/forward substitution)
 * @param m             Matrix to calculate the inverse of
 * @param d             Dimensionality of matrix
 * @param pr            Permute matrix rows on failure (true/false)
 * @return              Determinant of matrix
 */
double* lu_inv(double* m, int d) {

    /*
    Matrix Inversion
    To find the matrix inverse, we need to solve the equation AX=I , where X=inv(A)
    Let's decompose A=LU, so we have: (LU)X=I
    Now let UX=Y
    and thus LY=I
    This gives us two systems:
        first solve LY=I (it's easy because L is lower-triangular, so we just do the forward pass and obtain Y)
        then we solve UX=Y (it's also easy because U is upper-triangular)
    after that we get X which is our inverse

    if we need pivoting, then: we solve PAX=PI=P
    since PA=LU, we have LUX=P
    then we solve LY=P and UX=Y
    */

    // Declare variables
    int swaps = 0;
    bool status = FAIL;

    // Allocate memory
    double* l    = (double*) calloc(d*d,sizeof(double));
    double* u    = (double*) calloc(d*d,sizeof(double));
    double* yv   = (double*) calloc(d  ,sizeof(double));
    double* x    = (double*) calloc(d*d,sizeof(double));
    double* xv   = (double*) calloc(d  ,sizeof(double));
    double* Iv   = (double*) calloc(d  ,sizeof(double));

    if (l == NULL || u == NULL) {
        printf(PRT_ERROR"Failed to allocate memory for LU matrices \n");
        exit(EXIT_FAILURE);
    }

    if (x == NULL || yv == NULL || xv == NULL || Iv == NULL) {
        printf(PRT_ERROR"Failed to allocate memory for substitution matrices/vectors \n");
        exit(EXIT_FAILURE);
    }

    // Perform LU decomposition(s)
    status = _lu_decomp(m,d,l,u,false,&swaps);

    if (status == FAIL) {
        printf(PRT_WARN"LU decomposition failed after %d row permutations \n", swaps);
    } else {

        // Perform forward/backward substitutions to get inverse matrix
        for (int i = 0; i < d; i++) {       // For each column
            for (int j = 0; j < d; j++) {   // Setup column of I matrix
                Iv[j] = ( j == i ? 1.0F : 0.0F);
            }
            fSub(l,d,yv,Iv); // Get yv vector
            bSub(u,d,xv,yv); // Get xv vector
            for (int j = 0; j < d; j++) {   // Copy data to x from xv
                MADR(x,d,j,i) = xv[j];
            }
        }

    }

    // Tidy up
    free(l);  l  = NULL;
    free(u);  u  = NULL;
    free(yv); yv = NULL;
    free(xv); xv = NULL;
    free(Iv); Iv = NULL;

    return x;
    
}