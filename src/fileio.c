#include "mp.h"

/**
 * Load matrix from file
 * @param filename      Name of file to load
 * @param m             Pointer to store matrix
 * @param d             Dimensionality of matrix
 */
double* loadFile(char* filename, int* d){
    // Remove new line characters in filename, just in case
    for (int i = 0; i < STRLEN; i++){
		if(filename[i] == '\n'){
			filename[i] = '\0';
			break;
		}
	}

    // Check file permissions
	if (access(filename,F_OK) != 0){
		printf(PRT_ERROR"File doesn't exist \n");
        exit(EXIT_FAILURE);
	}
	if(access(filename,R_OK) != 0){
		printf(PRT_ERROR"Insufficient permissions for reading file \n");
        exit(EXIT_FAILURE);
	}

    // Open file in read only mode
    FILE *f = fopen(filename, "r"); 

	if (f == NULL) {
		printf(PRT_ERROR"Could not read file \n");
		exit(EXIT_FAILURE);
	}

    // Read dimensionality
	fscanf(f,"%d\n",d);

    if (*d < 1) {
        printf(PRT_ERROR"Error reading file: matrices must have a dimensionality > 0 \n");
        exit(EXIT_FAILURE);
    }

    printf(PRT_MSG"New matrix of dimension %d \n",*d);
    double* m = (double*) calloc((*d)*(*d),sizeof(double));

    if (m == NULL) {
        printf(PRT_ERROR"Failed to allocate memory for matrix \n");
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < (*d)*(*d); i++) {
        fscanf(f,"%lf\n",&m[i]);
    }

	fclose(f);

    return m;
}

/**
 * Save matrix to file
 * @param filename      Name of file to save
 * @param m             Matrix
 * @param d             Dimensionality of matrix
 */
void saveFile(char* filename, double* m, int d){
    printf("Saving matrix to '%s' \n",filename);

    // Remove new line characters
    for (int i = 0; i < STRLEN; i++){
		if(filename[i] == '\n'){
			filename[i] = '\0';
			break;
		}
	}

    // Check permissions
	if (access(filename,F_OK) == 0){
		printf(PRT_WARN"File already exists! Not overwriting \n");
        return;
	}

	FILE* f = fopen(filename,"w");

	fprintf(f,"%d\n",d);

	for (int j = 0; j < d*d; j++){
        fprintf(f,"%f\n",m[j]);
	}

	fclose(f);
}